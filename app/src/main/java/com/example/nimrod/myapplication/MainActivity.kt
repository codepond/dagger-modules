package com.example.nimrod.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.modulea.Foo
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var foo: Foo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMainComponent.builder().build()
    }
}
