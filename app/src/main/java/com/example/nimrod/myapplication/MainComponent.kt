package com.example.nimrod.myapplication

import com.example.modulea.AComponent
import dagger.Component

@Component(dependencies = [AComponent::class])
interface MainComponent {
    fun inject(mainActivity: MainActivity)
    fun libComponent(): AComponent
}