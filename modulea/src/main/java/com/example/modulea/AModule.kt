package com.example.modulea

import dagger.Binds
import dagger.Module

@Module
internal abstract class AModule {
    @Binds internal abstract fun bindFoobar(foobar: Foobar): Foo
}
