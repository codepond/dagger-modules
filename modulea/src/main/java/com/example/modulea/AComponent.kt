package com.example.modulea

import com.example.moduleb.BComponent
import dagger.Component

@Component(dependencies = [BComponent::class], modules = [AModule::class])
interface AComponent {
    fun foo(): Foo
}