package com.example.moduleb

import dagger.Component

@Component(modules = [BModule::class])
interface BComponent {
    fun cool(): Cool
}