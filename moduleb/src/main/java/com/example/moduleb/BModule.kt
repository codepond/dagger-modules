package com.example.moduleb

import dagger.Binds
import dagger.Module

@Module
abstract class BModule {
    @Binds abstract fun bindCoolio(coolio: Coolio): Cool
}
